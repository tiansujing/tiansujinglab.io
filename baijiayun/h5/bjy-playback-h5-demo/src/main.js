/**
 * @file 主模块
 * @author dujianhao
 */
define(function (require, exports) {
    var layout = require('./function/layout');
    var loading = require('./loading');
    var messagePanel = require('./messagePanel');
    var playerPanel = require('./playerPanel');
    var whiteboard = require('./whiteboard');
    var barrage = require('./barrage');
    var userPanel = require('./userPanel');

    var eventEmitter = BJY.eventEmitter;

    exports.init = function () {

        loading.init();

        eventEmitter
            .on(
                eventEmitter.CLASSROOM_CONNECT_FAIL,
                function () {
                    alert('网络已断开，请检查网络连接或者刷新页面重新进入房间');
                }
            )
            .on(
                eventEmitter.LOGIN_CONFLICT,
                function () {
                    alert('你已被踢，请确认用户 number 是否唯一或者刷新页面重新进入房间');
                }
            )
            .on(
                // Window resize 事件
                eventEmitter.WINDOW_RESIZE,
                function () {
                    layout();
                }
            );
        // 初始化房间
        BJY.playback.init({
	privateDomainPrefix:'tiansujing',    
        env: 'production',
            token: '6Q0p8jP_LJfg3M3qox2l6VFpEEtFR6mbjtoBdy87Ji9CYawF9349dLMqJ6hTQOllRn6gMta0cnEKp0fXMnVKLQ',
            class: {
                id: '18090462451266',
                sessionId:'201811161'
            },
            user: {
                number: '13147056',
                avatar: 'http://static.sunlands.com/newUserImagePath/13147056/40_40/13147056.jpg',
                name: '小苏',
                type: 0
            }
        })
            .then(function (data) {
                // 数据格式
                // var data = {
                //     'videoId': '11400557',
                //     'videos': {
                //         'low': [
                //             {
                //                 'cdn': 'csy',
                //                 'width': 320,
                //                 'height': 240,
                //                 'duration': 9124,
                //                 'url': 'http://dcsy-video.baijiayun.com/07c70230985976a1409fccf3b2498fe0/5b5ae102/00-x-upload/video/11400557_24a354dae3992dcff6c7bc614e2ab843_b2qX8oVk.mp4'
                //             },
                //             {
                //                 'cdn': 'al',
                //                 'width': 320,
                //                 'height': 240,
                //                 'duration': 9124,
                //                 'url': 'http://dal-video.baijiayun.com/07c70230985976a1409fccf3b2498fe0/5b5ae102/00-x-upload/video/11400557_24a354dae3992dcff6c7bc614e2ab843_b2qX8oVk.mp4'
                //             }
                //         ],
                //         'high': [
                //             {
                //                 'cdn': 'csy',
                //                 'width': 320,
                //                 'height': 240,
                //                 'duration': 9124,
                //                 'url': 'http://dcsy-video.baijiayun.com/07c70230985976a1409fccf3b2498fe0/5b5ae102/00-x-upload/video/11400557_24a354dae3992dcff6c7bc614e2ab843_b2qX8oVk.mp4'
                //             },
                //             {
                //                 'cdn': 'al',
                //                 'width': 320,
                //                 'height': 240,
                //                 'duration': 9124,
                //                 'url': 'http://dal-video.baijiayun.com/07c70230985976a1409fccf3b2498fe0/5b5ae102/00-x-upload/video/11400557_24a354dae3992dcff6c7bc614e2ab843_b2qX8oVk.mp4'
                //             }
                //         ]
                //     },
                //     'pages': [
                //         {
                //             'doc': {
                //                 'id': '1',
                //                 'name': '精讲一.pdf'
                //             },
                //             'page': 55,
                //             'time': 9052
                //         }
                //     ],
                //     'userVideos': {},
                //     'definition': [
                //         {
                //             'type': 'low',
                //             'name': '标清'
                //         },
                //         {
                //             'type': 'high',
                //             'name': '高清'
                //         }
                //     ],
                //     ],
                //     'defaultDefinition': 'low'
                // };
                var container = $(document.body);
                // 加载回放需要的各种组件，包括播放器，白板，用户列表等等
                userPanel.init(container);

                // 创建消息列表
                messagePanel.init(container);

                // 弹幕
                barrage.init(container);

                // 文档白板
                whiteboard.init(container);

                layout();

                // 播放器面板启动后回传video播放器
                var video = playerPanel.init(container, data);


            });
        // tab切换按钮事件
        var whiteboardBtn = $('#btn-whiteboard');
        var userListBtn = $('#btn-user-list');
        var messageListBtn = $('#btn-message-list');
        var tabContent = $('.tab-content');

        whiteboardBtn.on('click', function () {
            tabContent.css('margin-left', '0');

            whiteboardBtn.addClass('tab-select');

            messageListBtn.removeClass('tab-select');
            userListBtn.removeClass('tab-select');

        });

        userListBtn.on('click', function () {
            tabContent.css('margin-left', '-100vw');
            userListBtn.addClass('tab-select');

            whiteboardBtn.removeClass('tab-select');
            messageListBtn.removeClass('tab-select');

        });

        messageListBtn.on('click', function () {
            tabContent.css('margin-left', '-200vw');

            messageListBtn.addClass('tab-select');

            userListBtn.removeClass('tab-select');
            whiteboardBtn.removeClass('tab-select');
        });
    };
});
