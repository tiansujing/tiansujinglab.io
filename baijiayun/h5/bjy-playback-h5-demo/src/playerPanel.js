/**
 * @file 播放器面板
 * @author dujianhao
 */
define(function (require, exports) {
    var video;

    var eventEmitter = BJY.eventEmitter;
    var store = BJY.store;

    var onPlaying = function () {
        BJY.playback.play();
    };
    var onPause = function () {
        console.log('onpause');
        BJY.playback.pause();
    };
    var onSeeked = function () {
        console.log('onseeked');
        BJY.playback.seek(video.currentTime);
        BJY.playback.play();
        console.log(video.currentTime);
    };

    var onTimeUpdate = function () {
        BJY.playback.timeupdate(video.currentTime);
    };


    exports.init = function (element, data) {
        video = this.video = document.getElementById('bjy-player-teacher');

        BJY.playback.start();
        console.log(data);
        if (data.videos) {
            // 首先播默认分辨率
            video.src = data.videos[data.defaultDefinition][0].url;

            var initialized = false;

            element.find('#player-screen').on('click', function () {
                video.play();
                console.log(111);

            });

            video.oncanplay = function () {

                // 已启动过则不再执行
                if (initialized) {
                    return;
                }
                element.find('#player-screen').hide();
                initialized = true;
            };

            video.addEventListener('play', function () {
                onPlaying();
            });

            video.addEventListener('pause', function () {
                onPause();
            });

            video.addEventListener('seeked', function () {
                onSeeked();
            });

            // 必须监听视频的timeupdate事件，来告诉回放去根据视频时间更新数据
            video.addEventListener('timeupdate', function () {
                onTimeUpdate();
            });
        }

        store.watch('teacher.videoOn', function (videoOn) {
            console.log('老师摄像头是否开启', videoOn);
        });
        store.watch('teacher.audioOn', function (audioOn) {
            console.log('老师麦克风是否开启', audioOn);
        });

        // 监听
        eventEmitter.on(
            eventEmitter.SPEAKER_VOLUME_CHANGE,
            function (event, data) {
                // 声音值必须为百分比，即0-1之间的值
                var volume = data.value;
                if (video) {
                    video.volume = volume;
                }
            }
        );

        // 播放器面板启动后，回传播放器到主界面，供进度条使用
        return video;
    };
});