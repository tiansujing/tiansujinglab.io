/**
 * @file 主模块
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var layout = require('./function/layout');
    var isIOS = require('./function/isIOS');
    var loading = require('./loading');
    var messagePanel = require('./messagePanel');
    var playerPanel = require('./playerPanel');
    var whiteboard = require ('./whiteboard');
    var ui = require('./ui');

    var mediaData = BJY.data.media;
    var query = BJY.query;
    var eventEmitter = BJY.eventEmitter;
    var store = BJY.store;

    exports.init = function () {
        var element = $('#main');
        layout();

        ui.init();
        loading.init();
        playerPanel.init(element);
        eventEmitter
            .on(
                eventEmitter.CLASSROOM_CONNECT_FAIL,
                function () {
                    alert('网络已断开，请检查网络连接或者刷新页面重新进入房间');
                }
            )
            .on(
                eventEmitter.LOGIN_CONFLICT,
                function () {
                    alert('你已被踢，请确认用户 number 是否唯一或者刷新页面重新进入房间');
                }
            )

            // 监听初始化事件
            .one(
                eventEmitter.VIEW_RENDER_TRIGGER,
                function (event, data) {
                    mediaData.setSpeakerVolume(100);
                    // 初始化组件
                    whiteboard.init(element);
                    messagePanel.init(element);
                }
            );
        // 初始化房间
        BJY.init({
            env: 'production',
            sign: '5f4a3fabe0ebab2fdb1f2c09ddde8d94',
            class: {
                id: query.room_id || '18112789910666',
            },
            // 当前进入教室的用户的信息
            // 如果是老师进教室，传老师的信息
            // 如果是学生进教室，传学生的信息
            // number 必须确保是唯一的，如果不唯一，后进的用户会踢掉先进的用户
            user: {
                number: query.user_number || '0',
                avatar: query.user_avatar || 'https://img.baijiayun.com/0bjcloud/live/avatar/v2/121.jpg',
                name: query.user_name || '小平哥哥',
                type: '0'
            }
        });

        // tab切换按钮事件
        var TAB_MESSAGE = 0;
        var TAB_WHITEBOARD = 1;
        var tabIndex = TAB_MESSAGE;
        
        $('#btn-whiteboard').on('click',function () {
            $('.tab-content').css('margin-left', -1 * $(window).width());
            $('#btn-whiteboard').addClass('tab-select');
            $('#btn-message-list').removeClass('tab-select');

            tabIndex = TAB_WHITEBOARD;
        })
        $('#btn-message-list').on('click',function () {
            $('.tab-content').css('margin-left', '0');
            $('#btn-whiteboard').removeClass('tab-select');
            $('#btn-message-list').addClass('tab-select');

            tabIndex = TAB_MESSAGE;
        });
        
        // 设置ppt延迟buffer
        store.watch('teacher.audioOn', function (audioOn) {
            store.set('class.playBuffer', audioOn ? 11 : 0);
        });

        // 处理横竖屏的问题
        window.addEventListener('orientationchange', function(event) {
            if ( window.orientation == 180 || window.orientation == 0 ) {
                 //竖屏事件处理
            }
            if ( window.orientation == 90 || window.orientation == -90 ) {
                 //横屏事件处理
            }
            // 统一处理
            setTimeout(function () {
                // 延迟才能取到正确的width
                if (tabIndex == TAB_MESSAGE) {
                    $('.tab-content').css('margin-left', '0');
                }
                else if (tabIndex == TAB_WHITEBOARD){
                    $('.tab-content').css('margin-left', -1 * $(window).width());
                }
            }, 200);
        }, false);
    };
});
