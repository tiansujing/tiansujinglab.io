/**
 * @file 播放器面板
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var extension = typeof window.cefQuery === 'function' ? BJY.cef : BJY.Player.flash;
    var eventEmitter = BJY.eventEmitter;
    var store = BJY.store;
    var auth = BJY.auth;
    var userData = BJY.data.user;
    var myPlayer;
    var teacherPlayer;

    var foldHandler;

    function createMyPlayer(element) {
        var list = element.find('.list-section');
        myPlayer = BJY.DefaultThemePlayer.create({
            element: element.find('#live-self-player .placeholder'),
            replace: false,
            user: store.get('user'),
            extension: extension,
            canFold: true,
            canSwitchCamera: true,
            canSwitchMic: true,
            onComplete: function () {

            }
        });
        list.addClass('has-my-player');

        foldHandler = function (isFolded) {
            var element = $('#live-self-player');
            if (isFolded) {
                //执行折叠
                element.addClass('folded');
                list.removeClass('has-my-player');
            }
            else {
                // 执行展开
                element.removeClass('folded');
                list.addClass('has-my-player');
            }
        };
        myPlayer.watch('isFolded', foldHandler);
    }

    function createTeacherPlayer(element) {
        var teacher;
        if (auth.isTeacher()) {
            // 如果当前用户是老师
            teacher = store.get('user');
        }
        else {
            teacher = store.get('teacher');
        }
        if (!teacher) {
            teacher = {
                type: 1
            };
        }
        teacherPlayer = BJY.DefaultThemePlayer.create({
            element: element.find('#live-teacher-player .placeholder'),
            replace: false,
            user: store.get('presenter') || teacher,
            extension: extension,
            canSwitchCamera: auth.isSelf(store.get('teacher.id')),
            canSwitchMic: auth.isSelf(store.get('teacher.id'))
        });
    }

    exports.init = function (element) {

        if (auth.isTeacher()) {
            // 老师只创建老师的播放器
            createTeacherPlayer(element);
        }
        else {
            // 学生和助教创建老师和自己的播放器
            createTeacherPlayer(element);
            createMyPlayer(element);
        }

        eventEmitter
            .on(
                eventEmitter.MEDIA_SWITCH_TRIGGER,
                function (event, data) {
                    var player = BJY.Player.instances[BJY.store.get('user.id')];
                    /**
                     * 设置音视频设备的开关状态
                     *
                     * @param {Player} player 播放器实例
                     * @param {?boolean} videoOn 摄像头的开关状态
                     * @param {?boolean} audioOn 麦克风的开关状态
                     * @param {?boolean} release 是否重新推流
                     * @param {?boolean} disable 禁止发送信令
                     * @param {?number} actionType 操作类型 0: 手动开关摄像头和麦克风, 1: 上下课, 2: 改变摄像头设备, 3: 改变摄像头设置,
                     *                                    4: 改变麦克风设备, 5: 改变麦克风设置, 6: 切换链路类型
                     * @return {boolean|number} 操作成功返回 true，状态没变化返回 false，其它情况返回错误码
                     */
                    BJY.userPublish.setDevice(player, data.videoOn, data.audioOn, data.release, data.disable, data.actionType);
                }
            )
            .on(
                eventEmitter.ASSIST_MEDIA_SWITCH_TRIGGER,
                function (event, data) {
                    var player = BJY.Player.instances[BJY.store.get('user.id2')];
                    BJY.userPublish.setDevice(player, data.videoOn, false);
                }
            );

        store.watch('presenterId',function(newPresenterId, oldPresenterId) {

            var newPresenter = userData.find(newPresenterId);
            var oldPresenter = userData.find(oldPresenterId);

            // 主讲播放器销毁重建，
            if (newPresenter) {
                if (teacherPlayer) {
                    teacherPlayer.destroy();
                    teacherPlayer = null;
                    createTeacherPlayer(element);
                }
            }

            // 之前我是主讲，销毁重建我的播放器。
            if (store.get('user.id') === oldPresenterId) {
                if (myPlayer) {
                    if (foldHandler) {
                        myPlayer.unwatch('isFolded', foldHandler);
                        foldHandler = null;
                    }
                    myPlayer.destroy();
                    myPlayer = null;
                    createMyPlayer(element);
                }
            }

            // 之前我不是主讲 BJY.ActiveList.changePresenter(newPresenter, oldPresenter);
        });
    };
});