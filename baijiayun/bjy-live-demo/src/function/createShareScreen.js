/**
 * @function 上课按钮
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var language = require('../language/main')();
    var store = BJY.store;
    var auth = BJY.auth;
    var eventEmitter = BJY.eventEmitter;
    var createProgramDialog = require('./createProgramSelectDialog');

    /**
     * 触发开始分享
     * @param mode
     */
    function triggerStart(mode) {
        eventEmitter.trigger(
            eventEmitter.SCREEN_SHARE_START_TRIGGER,
            {
                mode: mode
            }
        );
    };

    /**
     * 可以分享条件校验
     *
     * @returns {boolean}
     */
    function checkCondition() {

        var myPlayer = BJY.Player.instances[store.get('user.id')];

        if (!auth.isClassStarted() || !auth.canShareScreen()) {
            tip({
                content: language.TIP_CLASS_NOT_START
            });
            return false;
        }

        if (!auth.canSpeak()) {
            tip({
                content: language.TIP_SPEAK_LIMIT
            });
            return false;
        }

        return true;
    }

    return function (element) {
        var onProgramClick;

        if (auth.canProgramShareDesktop()) {
            onProgramClick = function (mode, list) {
                if (checkCondition()) {
                    new createProgramDialog({
                        list: list,
                        mode: mode
                    });
                }
            };
        }

        var shareScreen = BJY.ScreenShareMenu.create({
            element: element,
            onFullScreenClick: function (mode) {
                if (checkCondition()) {
                    triggerStart(mode);
                }
            },
            onPartClick: function (mode) {
                if (checkCondition()) {
                    triggerStart(mode);
                }
            },
            onProgramClick: onProgramClick,
            onStopClick: function () {
                eventEmitter.trigger(
                    eventEmitter.SCREEN_SHARE_STOP_TRIGGER
                );
            }
        });
    }
});