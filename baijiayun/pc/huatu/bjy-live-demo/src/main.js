/**
 * @file 主模块
 * @author zhaogaoxing
 */
define(function (require, exports) {
    // 扩展自定义的权限，事件，配置
    require('./eventEmitter');
    require('./config');
    require('./auth');

    var ui = require('./ui');

    var loading = require('./loading');
    var messagePanel = require('./messagePanel');
    var playerPanel = require('./playerPanel');
    var settingPanel = require('./settingPanel');
    var whiteboard = require('./whiteboard');
    var barrage = require('./barrage');
    var userPanel = require('./userPanel');
    var documentPanel = require('./documentPanel');
    var preloadSWF = require('./function/preloadSWF');

    var query = BJY.query;
    var auth = BJY.auth;
    var eventEmitter = BJY.eventEmitter;
    var extension = BJY.getExtension();
    extension.pluginUrl = location.protocol + '//live-cdn.baijiayun.com/js-sdk/latest/player/extension/flash.swf';


    exports.init = function (data) {
        ui.init();
        loading.init();

        eventEmitter
            .one(
                eventEmitter.CLASSROOM_CONNECT_TRIGGER,
                function (event, data) {
                    // 初始化扩展
                    extension.init().then(function () {
                        eventEmitter.trigger(
                            eventEmitter.CLASSROOM_CONNECT_TRIGGER,
                            data
                        );
                    });
                    return false;
                }
            )
            .on(
                eventEmitter.CLASSROOM_CONNECT_FAIL,
                function () {
                    alert('网络已断开，请检查网络连接或者刷新页面重新进入房间');
                }
            )
            .on(
                eventEmitter.LOGIN_CONFLICT,
                function () {
                    alert('你已被踢，请确认用户 number 是否唯一或者刷新页面重新进入房间');
                }
            )

            // 监听初始化事件
            .one(
                eventEmitter.VIEW_RENDER_TRIGGER,
                function (event, data) {
                    // 初始化组件
                    var element = $('#main');

                    if (auth.isChromeSpecialVersion()) {
                        preloadSWF.init();
                    }

                    playerPanel.init(element);
                    whiteboard.init(element);
                    // barrage.init(element);
                    settingPanel.init(element);
                    userPanel.init(element);
                    messagePanel.init(element);

                    if (auth.isTeacher() || auth.isAssistant()) {
                        documentPanel.init(element.find('.left'));
                    }
                    eventEmitter.trigger(
                        eventEmitter.DOC_ALL_REQ
                    );

                    if (auth.isTeacher()) {
                        $('body').addClass('teacher');
                    }
                    if (auth.isAssistant()) {
                        $('body').addClass('assistant');
                    }
                    if (auth.isStudent()) {
                        $('body').addClass('student');
                    }
                }
            );
        // 初始化房间
        // speakState 设置当前用户的发言权限，如果想要自由发言设为0，举手发言设为1，你可以根据class.type 或者其他的一些条件来确定speakState的值，不传它的默认值为0.
        // 老师和助教设为自由发言
        if (auth.isTeacher(data.user.type) || auth.isAssistant(data.user.type)) {
            data.class.speakState = 0;
        }
        // 一对一和小班课设为自由发言
        else if (auth.isOneToOne(data.class.type) || auth.isMini(data.class.type)) {
            data.class.speakState = 0;
        }
        else {
            data.class.speakState = 1;
        }

        BJY.init({
            env: data.env || 'production',
            sign: data.sign,
            class: data.class,
            // 当前进入教室的用户的信息
            // 如果是老师进教室，传老师的信息
            // 如果是学生进教室，传学生的信息
            // number 必须确保是唯一的，如果不唯一，后进的用户会踢掉先进的用户
            user: data.user,
            // 如果知道老师的信息，传入，如果没有，传入{type: 1}
            teacher: data.teacher
        });
        // tab切换按钮事件
        $('#btn-user-list').on('click',function () {
            $('.tab-content').css('margin-left', -532);
            $('#btn-user-list').addClass('tab-select');
            $('#btn-action-list').removeClass('tab-select');
            $('#btn-message-list').removeClass('tab-select');

        });
        $('#btn-message-list').on('click',function () {
            $('.tab-content').css('margin-left', -266);
            $('#btn-message-list').addClass('tab-select').removeClass('has-new');
            $('#btn-user-list').removeClass('tab-select');
            $('#btn-action-list').removeClass('tab-select');
        });
        $('#btn-action-list').on('click',function () {
            $('.tab-content').css('margin-left', 0);
            $('#btn-action-list').addClass('tab-select').removeClass('has-new');
            $('#btn-user-list').removeClass('tab-select');
            $('#btn-message-list').removeClass('tab-select');
        });
    };
});