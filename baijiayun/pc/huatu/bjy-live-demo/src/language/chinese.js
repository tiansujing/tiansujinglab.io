/**
 * @file 语言包
 * @author zhaogaoxing
 */
define(function (require, exports, module) {

    'use strict';

    exports.LABEL_CONFIRM = '确定';

    exports.TIME_SECONED = '秒';


    /**
     * =================================================
     * 用户角色
     * =================================================
     */

    exports.USER_ROLE_TEACHER = '老师';

    exports.USER_ROLE_STUDENT = '学生';

    exports.USER_ROLE_GUEST = '游客';

    exports.USER_ROLE_ASSISTANT = '助教';

    /**
     * =================================================
     * 上下课
     * =================================================
     */
    exports.CLASS_START = '上课';

    exports.CLASS_END = '下课';

    exports.TIP_CLASS_START = '已上课';

    exports.TIP_CLASS_END = '已下课';


    exports.TIP_CLASS_NOT_START = '课程未开始，请先点击上课按钮';

    exports.TIP_SPEAK_LIMIT = '发言请先举手';

    exports.TIP_SPEAK_APPLY_TIMEOUT = '老师未做响应，已自动取消申请发言';

    exports.TIP_SPEAK_APPLY_REJECT = '老师拒绝了你的发言申请';

    exports.TIP_SPEAK_APPLY_ACCEPT = '老师已允许你发言';

    exports.DIALOG_TITLE_DOCUMENT = '课件管理';

    exports.DIALOG_TITLE_PROGRAM = '选择分享的程序';

    exports.BUTTON_CONTENT_DOCUMENT = '课件管理';

    exports.BUTTON_CONTENT_MY_DOCUMENT = '我的课件';

    exports.BUTTON_CONTENT_FULLSCREEN = '全屏';

    exports.BUTTON_CONTENT_FULLSCREEN_EXIT = '退出全屏';

    exports.BUTTON_ROLL_CALL = '点名';

    exports.ROLL_CALL_STUDENT_TIME = '请输入学生响应时间: ';

    exports.ROLL_CALL_TEACHER_TIME = '秒后查看点名结果';

    exports.ROLL_CALL_STUDENT_RES = '老师发起了点名，请点击确定，剩余: ';

    exports.ROLL_CALL_SUCCESS = '已签到';

    exports.ROLL_CALL_FINISH = '点名结束';

    exports.ROLL_CALL_RESULT = '查看结果';

    exports.ROLL_CALL_AGAIN = '再来一次';

    exports.TITLE_ROLL_CALL_RESULT = '点名结果';

    exports.ROLL_CALL_YES = '已签到';

    exports.ROLL_CALL_NO = '未签到';

});