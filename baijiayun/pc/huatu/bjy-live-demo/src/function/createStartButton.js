/**
 * @function 上课按钮
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var getLanguage = require('../language/main');
    var store = BJY.store;
    var auth = BJY.auth;
    var eventEmitter = BJY.eventEmitter;
    return function (element) {
        var language = getLanguage();
        if (store.get('class.started')) {
            element.text(language.CLASS_END);
        }
        else {
            element.text(language.CLASS_START);
        }
        element.on('click', function (e) {
            if (auth.isTeacher()) {
                if (store.get('class.started')) {
                    eventEmitter.trigger(
                        eventEmitter.CLASS_END_TRIGGER
                    );
                    element.text(language.CLASS_START);
                    tip({
                        content: language.TIP_CLASS_END
                    });
                }
                else {
                    eventEmitter.trigger(
                        eventEmitter.CLASS_START_TRIGGER
                    );
                    element.text(language.CLASS_END);
                    tip({
                        content: language.TIP_CLASS_START
                    });
                    if (auth.isTeacher()) {
                        // 老师上课自动打开音视频
                        eventEmitter.trigger(
                            eventEmitter.MEDIA_SWITCH_TRIGGER,
                            {
                                videoOn: true,
                                audioOn: true
                            }
                        );
                    }
                }
            }
        });
    }
});