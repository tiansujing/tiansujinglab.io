/**
 * @function 上课按钮
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var getLanguage = require('../language/main');
    var store = BJY.store;
    var auth = BJY.auth;
    var eventEmitter = BJY.eventEmitter;
    return function (element) {
        var language = getLanguage();
        var record = BJY.Record.create({
            element: element,
            // 是否可以快捷键
            canUseHotKey: true,
            // 停止本地录制的快捷键
            stopLocalRecordHotKey: 'ctrl + s',
            // 是否可以云端录制
            canUseCloudRecord: true,
            // 是否可以本地录制
            canUseLocalRecord: false,
            // 是否可以管理云端回放
            canManagePlayback: false ,
            // 云端回放点击回调
            onManagePlaybackClick: function () {
                var url = is.xClass()
                                ? 'http://i.genshuixue.com/jigou2/main.html#/courseList'
                                : '/teacher_center/cloudplayback';
                window.open(url);
            },
            // 开始云端录制回调
            onStartCloudRecordClick: function () {
                record.startCloudRecord();
            },
            // 结束云端录制回调
            onEndCloudRecordClick: function () {
                record.endCloudRecord();
            },
            // 开始本地录制回调
            onStartLocalRecordClick: function () {
                record.startLocalRecord({
                    width: 320,
                    height: 400
                });
            },
            // 结束本地录制回调
            onEndLocalRecordClick: function () {
                record.endLocalRecord();
            },
            // 暂停本地录制回调
            onPauseLocalRecord: function () {
                record.pauseLocalRecord();
            },
            // 重新开始本地录制回调
            onRestartLocalRecord: function () {
                record.restartLocalRecord();
            }
        });
    }
});