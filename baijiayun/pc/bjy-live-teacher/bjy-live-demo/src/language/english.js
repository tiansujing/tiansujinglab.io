/**
 * @file 语言包
 * @author zhaogaoxing
 */
define(function (require, exports, module) {

    'use strict';

    /**
     * =================================================
     * 用户角色
     * =================================================
     */

    exports.USER_ROLE_TEACHER = 'Teacher';

    exports.USER_ROLE_STUDENT = 'Student';

    exports.USER_ROLE_GUEST = 'TA';

    exports.USER_ROLE_ASSISTANT = 'Guest';

});
