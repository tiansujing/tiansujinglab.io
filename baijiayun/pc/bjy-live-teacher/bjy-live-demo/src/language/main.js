/**
 * @file 话术入口
 * @author zhaogaoxing
 */
define(function (require, exports) {

    'use strict';

    return function () {
        var store = BJY.store;
        var chinese = require('./chinese');
        var english = require('./english');
        return store.get('language') === 'english' ? english : chinese;
    };
});