/**
 * @file 设置面板
 * @author zhaogaoxing
 */
define(function (require, exports) {
    var eventEmitter = BJY.eventEmitter;
    var store = BJY.store;
    var auth = BJY.auth;
    var config = BJY.config
    var createStartButton = require('./function/createStartButton');
    var createRecord = require('./function/createRecord');
    var createShareScreen = require('./function/createShareScreen');
    var createRollCall = require('./function/rollCallTeacher');
    var rollCallStudent = require('./function/rollCallStudent');
    var language = require('./language/main')();

    var isFullscreen = false;

    function toggleFullscreen(element) {
        var icon = element.find('.icon');
        eventEmitter.trigger(
            isFullscreen
            ? eventEmitter.FULLSCREEN_EXIT_TRIGGER
            : eventEmitter.FULLSCREEN_ENTER_TRIGGER
        );
        if (isFullscreen) {
            element.find('span').text(language.BUTTON_CONTENT_FULLSCREEN);
            icon.removeClass('icon-compress');
            icon.addClass('icon-expand');
        }
        else {
            element.find('span').text(language.BUTTON_CONTENT_FULLSCREEN_EXIT);
            icon.removeClass('icon-expand');
            icon.addClass('icon-compress');
        }
        isFullscreen = !isFullscreen;
    }

    exports.init = function (element) {
        if (!store.get('class.isFree')
            && auth.isStudent()
        ) {
            var speakApplyMenu = BJY.SpeakApplyMenu.create({
                element: element.find('#menu-speak-apply'),
                // 举手按钮被点击
                onApplyClick: function () {
                    BJY.userSpeak.startApply(10 * 1000);
                },
                // 取消举手被点击
                onCancelClick: function () {
                    BJY.userSpeak.cancelApply();
                },
                // 结束发言被点击
                onStopClick: function () {
                    BJY.userSpeak.stopSpeak(BJY.store.get('user.id'));
                }
            });

            eventEmitter
            .on(
                eventEmitter.SPEAK_APPLY_RESULT_TIMEOUT,
                function (e, data) {
                    tip({
                        content: language.TIP_SPEAK_APPLY_TIMEOUT
                    });
                }
            )
            .on(
                eventEmitter.SPEAK_APPLY_RESULT_REJECT,
                function (e, data) {
                    tip({
                        content: language.TIP_SPEAK_APPLY_REJECT
                    });
                }
            )
            .on(
                eventEmitter.SPEAK_APPLY_RESULT_ACCEPT,
                function (e, data) {
                    tip({
                        content: language.TIP_SPEAK_APPLY_ACCEPT
                    });
                }
            );
        }
        // 创建设置按钮
        var speakerMenu = BJY.SpeakerMenu.create({
            element: element.find('#menu-speaker'),
            canAdjustVolume: true,
            maxVolume: 100
        });
        var micMenu = BJY.MicMenu.create({
            element: element.find('#menu-mic'),
            maxVolume: 100,
            canSelectDevice: true,
            canAdjustVolume: true,
            onSwitcherClick: function () {
                if (!store.get('class.started')) {
                    tip({
                        content: language.TIP_CLASS_NOT_START
                    });
                    return;
                }
                if (store.get('class.speakState') == config.SPEAK_STATE_LIMIT) {
                    tip({
                        content: language.TIP_SPEAK_LIMIT
                    });
                    return;
                }
                // 获取我的播放器
                var player = BJY.Player.instances[BJY.store.get('user.id')];
                // 设置麦克风设备
                BJY.userPublish.setDevice(player, null, !player.audioOn);
            }
        });
        var cameraMenu = BJY.CameraMenu.create({
            element: element.find('#menu-camera'),
            canSelectDevice: true,
            onSwitcherClick: function () {
                if (!store.get('class.started')) {
                    tip({
                        content: language.TIP_CLASS_NOT_START
                    });
                    return;
                }
                if (store.get('class.speakState') == config.SPEAK_STATE_LIMIT) {
                    tip({
                        content: language.TIP_SPEAK_LIMIT
                    });
                    return;
                }
                // 获取我的播放器
                var player = BJY.Player.instances[BJY.store.get('user.id')];
                // 设置麦克风设备
                BJY.userPublish.setDevice(player, !player.videoOn);
            }
        });

        // 上课按钮
        if (auth.isTeacher() || auth.isAssistant()) {
            createStartButton(element.find('#btn-start'));
            createRecord(element.find('#menu-record'));
        }

        // 创建全屏按钮
        var fullscreenElement = element.find('#btn-fullscreen');
        fullscreenElement.on('click', function (e) {
            toggleFullscreen(fullscreenElement);
        });

        // 屏幕分享
        if (auth.canShareScreen()) {
            createShareScreen(element.find('#menu-share-screen'));
        }
        else {
            element.find('#menu-share-screen').remove();
        }

        // 点名
        if (auth.isTeacher() || auth.isAssistant()) {
            createRollCall(element.find('#menu-roll-call'));
        }
        else {
            element.find('#menu-roll-call').remove();
            rollCallStudent();
        }
    };
});