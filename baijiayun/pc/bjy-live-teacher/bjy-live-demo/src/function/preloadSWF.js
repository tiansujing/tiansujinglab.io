/**
 * @file flash 授权预加载
 * @author zhaogaoxing
 *
 * flash 授权弹框有个奇怪的特性，当浏览器只有当前标签页引入flash时
 * 点击允许后，再刷新页面，会记住允许的选项
 * 如果在 flash 之前再引入一个 flash 就可以解决这个问题
 */
define(function (require, exports) {

    'use strict';

    exports.init = function () {

        var url = '//live-cdn.baijiayun.com/js-sdk/0.1.36/player/extension/flash.swf';
        var html = BJY.createSWF('temp', '', url);

        var swf = $(html);
        $('body').prepend(swf);
        swf.css({
            position: 'absolute',
            top: 0,
            width: 398,
            height: 298,
            opacity: 0
        });
    }
});